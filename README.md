# README #

This application shows jokes taken from [https://api.chucknorris.io](https://api.chucknorris.io). Here are some screenshots from the application:

![Splash screen](https://farmasikita.my.id/img/test/chucknorris.io_splash.png) ![Category list](https://farmasikita.my.id/img/test/chucknorris.io_category_list.png) ![Search progress](https://farmasikita.my.id/img/test/chucknorris.io_search_list.png) ![Joke detail](https://farmasikita.my.id/img/test/chucknorris.io_detail.png) ![Loading animation](https://farmasikita.my.id/img/test/chucknorris.io_loading.png)

### Implements ###

The application implements the following:

* [Java/JDK 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
* [ButterKnife](https://jakewharton.github.io/butterknife)
* [Data Binding](https://developer.android.com/topic/libraries/data-binding)
* [Glide](https://github.com/bumptech/glide)
* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
* [MVVM](https://developer.android.com/jetpack/guide)
* [Retrofit](https://square.github.io.retrofit/)

### Disclaimer ###

All writings, jokes and image assets are taken from [https://api.chucknorris.io](https://api.chucknorris.io).