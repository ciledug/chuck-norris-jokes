package id.my.farmasikita.jokesonyou.utils;

public class AppConstants {

    public static final String EXTRA_BUNDLE = "extra_bundle";
    public static final String EXTRA_SELECTED_CATEGORY = "extra_selected_category";
    public static final String EXTRA_SELECTED_JOKE = "extra_selected_joke";

    public static final String TAG_PROGRESS = "tag_progress";

    public static final int SEARCH_TEXT_INPUT_LENGTH = 10;
    public static final String DATE_FORMAT_COMMON = "yyyy-MM-dd HH:mm:ss";
}
