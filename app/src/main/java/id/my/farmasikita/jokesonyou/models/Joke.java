package id.my.farmasikita.jokesonyou.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Joke implements Serializable {

    @SerializedName("icon_url")
    private String mIconUrl = "";

    @SerializedName("id")
    private String mId = "";

    @SerializedName("url")
    private String mUrl = "";
    
    @SerializedName("value")
    private String mValue = "";

    @SerializedName("categories")
    private CategoryList mCategories;

    @SerializedName("created_at")
    private String mCreatedAt = "";

    @SerializedName("updated_at")
    private String mUpdatedAt = "";

    public Joke() {
    }
    
    public String getIconUrl() {
        return mIconUrl;
    }

    public void setIconUrl(String mIconUrl) {
        this.mIconUrl = mIconUrl;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String mValue) {
        this.mValue = mValue;
    }

    public CategoryList getCategories() {
        return mCategories;
    }

    public void setCategories(CategoryList mCategories) {
        this.mCategories = mCategories;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    @Override
    public String toString() {
        StringBuilder tempString = new StringBuilder("\n--- Joke ---\n");
        tempString.append("mId: ").append(mId).append("\n");
        tempString.append("mIconUrl: ").append(mIconUrl).append("\n");
        tempString.append("mUrl: ").append(mUrl).append("\n");
        tempString.append("mCategories: ").append(mCategories.getCategories().size()).append("\n");
        tempString.append("mCreatedAt: ").append(mCreatedAt).append("\n");
        tempString.append("mUpdatedAt: ").append(mUpdatedAt).append("\n");
        return tempString.toString();
    }
}
