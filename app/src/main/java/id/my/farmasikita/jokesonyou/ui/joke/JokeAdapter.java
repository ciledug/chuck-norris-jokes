package id.my.farmasikita.jokesonyou.ui.joke;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.my.farmasikita.jokesonyou.R;
import id.my.farmasikita.jokesonyou.databinding.ItemJokeBinding;
import id.my.farmasikita.jokesonyou.models.Joke;
import id.my.farmasikita.jokesonyou.utils.AppConstants;
import id.my.farmasikita.jokesonyou.utils.Tools;

public class JokeAdapter extends RecyclerView.Adapter<JokeAdapter.JokeAdapterViewHolder> {

    private final String TAG = getClass().getSimpleName();

    public interface JokeAdapterListener {
        void onJokeClicked(Joke joke);
    }

    private Context mContext;
    private List<Joke> mJokeList = new ArrayList<>();
    private JokeAdapterListener mListener = null;
    private SimpleDateFormat mSimpleDateFormat;

    public JokeAdapter(Context context, List<Joke> jokeList, JokeAdapterListener listener) {
        this.mContext = context;
        this.mJokeList = jokeList;
        this.mListener = listener;
        this.mSimpleDateFormat = new SimpleDateFormat(AppConstants.DATE_FORMAT_COMMON, Locale.getDefault());
    }

    @NonNull
    @Override
    public JokeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemJokeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_joke, parent, false);
        return new JokeAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull JokeAdapterViewHolder holder, int position) {
        if (position == 0) {
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) holder.itemBinding.cvJokeBodyContainer.getLayoutParams();
            lp.setMargins(lp.leftMargin, lp.bottomMargin, lp.rightMargin, lp.bottomMargin);
            holder.itemBinding.cvJokeBodyContainer.setLayoutParams(lp);
        }
        Joke joke = mJokeList.get(position);

        holder.itemBinding.tvValue.setText(joke.getValue());
        Tools.setImage(mContext, holder.itemBinding.ivJokeIcon, joke.getIconUrl());
        holder.itemBinding.cvJokeBodyContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onJokeClicked(joke);
                }
            }
        });

        try {
            holder.itemBinding.tvUpdatedAt.setText(mSimpleDateFormat.format(mSimpleDateFormat.parse(joke.getUpdatedAt())));
        } catch (ParseException | NullPointerException e) {
        }
    }

    @Override
    public int getItemCount() {
        return mJokeList.size();
    }

    public void refreshList(List<Joke> jokeList) {
        this.mJokeList.clear();
        this.mJokeList.addAll(jokeList);
        notifyDataSetChanged();
    }

    public class JokeAdapterViewHolder extends RecyclerView.ViewHolder {

        ItemJokeBinding itemBinding;

        public JokeAdapterViewHolder(ItemJokeBinding binding) {
            super(binding.getRoot());
            itemBinding = binding;
        }
    }

}
