package id.my.farmasikita.jokesonyou.ui.category;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import id.my.farmasikita.jokesonyou.models.CategoryList;
import id.my.farmasikita.jokesonyou.repository.DataRepository;

public class CategoryViewModel extends ViewModel {

    private final String TAG = getClass().getSimpleName();

    private DataRepository mDataRepository;
    private MutableLiveData<CategoryList> mCategories;

    public CategoryViewModel() {
        mDataRepository = DataRepository.getInstance();
    }

    public LiveData<CategoryList> requestCategories() {
        if (mCategories == null) {
            mCategories = mDataRepository.requestCategories();
        }
        return mCategories;
    }
}
