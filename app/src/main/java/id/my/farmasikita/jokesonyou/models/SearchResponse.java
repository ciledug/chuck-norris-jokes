package id.my.farmasikita.jokesonyou.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchResponse {

    @SerializedName("total")
    private int mTotal = 0;

    @SerializedName("result")
    private List<Joke> mResult = new ArrayList<>();

    public SearchResponse() {
    }

    public int getTotal() {
        return mTotal;
    }

    public void setTotal(int mTotal) {
        this.mTotal = mTotal;
    }

    public List<Joke> getResult() {
        return mResult;
    }

    public void setResult(List<Joke> mResult) {
        this.mResult = mResult;
    }
}
