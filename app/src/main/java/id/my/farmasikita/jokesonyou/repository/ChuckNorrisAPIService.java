package id.my.farmasikita.jokesonyou.repository;

import id.my.farmasikita.jokesonyou.models.CategoryList;
import id.my.farmasikita.jokesonyou.models.Joke;
import id.my.farmasikita.jokesonyou.models.SearchResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ChuckNorrisAPIService {

    String developmentURL = "https://api.chucknorris.io/";
    int TIMEOUT = 10;
    int READ_TIMEOUT = 10;

    @GET("jokes/categories")
    Call<CategoryList> requestCategories();

    @GET("jokes/random")
    Call<Joke> requestRandom();

    @GET("jokes/random")
    Call<Joke> requestRandom(@Query("category") String category);

    @GET("jokes/search")
    Call<SearchResponse> requestSearch(@Query("query") String query);
}
