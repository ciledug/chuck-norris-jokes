package id.my.farmasikita.jokesonyou.ui.category;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.my.farmasikita.jokesonyou.R;
import id.my.farmasikita.jokesonyou.fragments.ProgressDialogFragment;
import id.my.farmasikita.jokesonyou.models.CategoryList;
import id.my.farmasikita.jokesonyou.models.Joke;
import id.my.farmasikita.jokesonyou.ui.joke.JokeDetailActivity;
import id.my.farmasikita.jokesonyou.ui.joke.SearchJokeActivity;
import id.my.farmasikita.jokesonyou.utils.AppConstants;

public class CategoryListActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private CategoryAdapter mAdapter;
    private CategoryViewModel mViewModel;
    private MutableLiveData<CategoryList> mCategoryList = new MutableLiveData<>();

    private Joke mSelectedJoke = null;
    private String mSelectedCategory = "";
    private ProgressDialogFragment mProgressDialog;

    private CategoryAdapter.CategoryAdapterListener mCategoryAdapterListener = new CategoryAdapter.CategoryAdapterListener() {
        @Override
        public void onCategoryClicked(String categoryName) {
            mSelectedCategory = categoryName;
            gotoJokeDetail();
        }
    };

    @BindView(R.id.tbToolbar)
    Toolbar tbToolbar;

    @BindView(R.id.rvCategoryListContainer)
    RecyclerView rvCategoryListContainer;

    @OnClick(R.id.fab_random_joke)
    public void onFabClicked() {
        mSelectedCategory = "";
        gotoJokeDetail();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ButterKnife.bind(this);

        mViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        mProgressDialog = ProgressDialogFragment.newInstance();

        requestCategoryList();
        initScreen();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_menu_search) {
            gotoSearchPage();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initScreen() {
        setSupportActionBar(tbToolbar);
        tbToolbar.setNavigationIcon(null);
        rvCategoryListContainer.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        );
    }

    private void requestCategoryList() {
        mProgressDialog.show(getSupportFragmentManager(), AppConstants.TAG_PROGRESS);

        mViewModel.requestCategories().observe(this, new Observer<CategoryList>() {
            @Override
            public void onChanged(CategoryList categoryList) {
                if (mAdapter == null) {
                    mAdapter = new CategoryAdapter(getApplicationContext(), mViewModel.requestCategories().getValue(), mCategoryAdapterListener);
                    rvCategoryListContainer.setAdapter(mAdapter);
                }

                mCategoryList.setValue(categoryList);
                mAdapter.addItems(mCategoryList.getValue().getCategories());
                mProgressDialog.dismiss();
            }
        });
    }

    private void gotoSearchPage() {
        Intent i = new Intent(this, SearchJokeActivity.class);
        startActivity(i);
    }

    public void gotoJokeDetail() {
        Bundle b = new Bundle();
        b.putSerializable(AppConstants.EXTRA_SELECTED_JOKE, mSelectedJoke);
        b.putString(AppConstants.EXTRA_SELECTED_CATEGORY, mSelectedCategory);

        Intent i = new Intent(this, JokeDetailActivity.class);
        i.putExtra(AppConstants.EXTRA_BUNDLE, b);
        startActivity(i);
    }

}