package id.my.farmasikita.jokesonyou.repository;

import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import id.my.farmasikita.jokesonyou.models.CategoryList;
import id.my.farmasikita.jokesonyou.models.Joke;
import id.my.farmasikita.jokesonyou.models.SearchResponse;
import id.my.farmasikita.jokesonyou.utils.ChuckNorrisRetrofitUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChuckNorrisAPIClient {

    private final String TAG = getClass().getSimpleName();

    private static ChuckNorrisAPIClient apiClient = null;
    private ChuckNorrisAPIService mApiService = null;
    private MutableLiveData<Joke> mJoke;
    private MutableLiveData<CategoryList> mCategories;
    private MutableLiveData<SearchResponse> mSearchResponse;

    public ChuckNorrisAPIClient(ChuckNorrisAPIService apiService) {
        mApiService = apiService;
    }

    public static ChuckNorrisAPIClient getInstance() {
        synchronized (ChuckNorrisAPIClient.class) {
            if (apiClient == null) {
                apiClient = new ChuckNorrisAPIClient(ChuckNorrisRetrofitUtil.create());
            }
        }
        return apiClient;
    }

    public MutableLiveData<CategoryList> requestCategories() {
        MutableLiveData<CategoryList> data = new MutableLiveData<>();
        mApiService.requestCategories()
            .enqueue(new Callback<CategoryList>() {
                @Override
                public void onResponse(Call<CategoryList> call, Response<CategoryList> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        data.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CategoryList> call, Throwable t) {
                    data.setValue(new CategoryList());
                }
            });
        return data;
    }

    public MutableLiveData<Joke> requestRandomJoke(String category) {
        MutableLiveData<Joke> data = new MutableLiveData<>();
        Call<Joke> callJoke;

        if (TextUtils.isEmpty(category)) {
            callJoke = mApiService.requestRandom();
        } else {
            callJoke = mApiService.requestRandom(category);
        }

        callJoke.enqueue(new Callback<Joke>() {
            @Override
            public void onResponse(Call<Joke> call, Response<Joke> response) {
                if (response.isSuccessful() && response.body() != null) {
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Joke> call, Throwable t) {
                Log.e(TAG + ".onFailure", "t: " + t.getMessage());
            }
        });
        return data;
    }

    public MutableLiveData<List<Joke>> requestSearch(String query) {
        MutableLiveData<List<Joke>> jokes = new MutableLiveData<>();
        mApiService.requestSearch(query)
            .enqueue(new Callback<SearchResponse>() {
                @Override
                public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                    if (response.isSuccessful() && (response.body() != null) && (response.body().getTotal() > 0)) {
                        jokes.setValue(response.body().getResult());
                    } else {
                        jokes.setValue(new ArrayList<>());
                    }
                }

                @Override
                public void onFailure(Call<SearchResponse> call, Throwable t) {
                    Log.e(TAG + ".onFailure", "" + t.getMessage());
                    jokes.setValue(new ArrayList<>());
                }
            });
        return jokes;
    }
}
