package id.my.farmasikita.jokesonyou.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryList implements Serializable {

    @SerializedName("categories")
    private List<String> mCategories = new ArrayList<>();

    public CategoryList() {
    }

    public List<String> getCategories() {
        return mCategories;
    }

    public void setCategories(List<String> mCategories) {
        this.mCategories = mCategories;
    }
}