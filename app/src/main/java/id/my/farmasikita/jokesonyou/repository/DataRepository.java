package id.my.farmasikita.jokesonyou.repository;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import id.my.farmasikita.jokesonyou.models.CategoryList;
import id.my.farmasikita.jokesonyou.models.Joke;

public class DataRepository {

    private final String TAG = getClass().getSimpleName();

    private static DataRepository mDataRepository = null;
    private static ChuckNorrisAPIClient mApiClient = null;
//    private static ChuckNorrisDaoClient mDaoClient = null;

    private DataRepository() {
        mApiClient = ChuckNorrisAPIClient.getInstance();
    }

    public static DataRepository getInstance() {
        synchronized (DataRepository.class) {
            if (mDataRepository == null) {
                mDataRepository = new DataRepository();
            }
        }
        return mDataRepository;
    }

    public MutableLiveData<CategoryList> requestCategories() {
        return mApiClient.requestCategories();
    }

    public MutableLiveData<Joke> requestRandomJoke(String category) {
        return mApiClient.requestRandomJoke(category);
    }

    public MutableLiveData<List<Joke>> requestSearch(String query) {
        return mApiClient.requestSearch(query);
    }
}
