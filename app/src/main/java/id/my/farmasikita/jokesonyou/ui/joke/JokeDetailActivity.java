package id.my.farmasikita.jokesonyou.ui.joke;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.my.farmasikita.jokesonyou.R;
import id.my.farmasikita.jokesonyou.databinding.ActivityJokeDetailBinding;
import id.my.farmasikita.jokesonyou.fragments.ProgressDialogFragment;
import id.my.farmasikita.jokesonyou.models.Joke;
import id.my.farmasikita.jokesonyou.utils.AppConstants;
import id.my.farmasikita.jokesonyou.utils.Tools;

public class JokeDetailActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private MutableLiveData<Joke> mSelectedJoke = new MutableLiveData<>();
    private ActivityJokeDetailBinding mBinding;
    private JokeViewModel mJokeViewModel;
    private String mSelectedCategory = "";
    private ProgressDialogFragment mProgressDialog;

    @BindView(R.id.tbToolbar)
    Toolbar tbToolbar;

    @BindView(R.id.ivJokeIconDetail)
    ImageView ivJokeIconDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_joke_detail);
        mJokeViewModel = new ViewModelProvider(this).get(JokeViewModel.class);
        mProgressDialog = ProgressDialogFragment.newInstance();
        ButterKnife.bind(this);

        if (getIntent().hasExtra(AppConstants.EXTRA_BUNDLE)) {
            Bundle b = getIntent().getBundleExtra(AppConstants.EXTRA_BUNDLE);
            if (b != null) {
                mSelectedJoke.setValue((Joke) b.getSerializable(AppConstants.EXTRA_SELECTED_JOKE));
                mSelectedCategory = b.getString(AppConstants.EXTRA_SELECTED_CATEGORY);

                if (mSelectedJoke.getValue() != null) {
                    showJoke();
                }
                else {
                    getRandomJoke();
                }
            }
        }

        initScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mJokeViewModel.getJoke() != null) {
            mSelectedJoke.setValue(mJokeViewModel.getJoke().getValue());
            showJoke();
        }
    }

    @OnClick(R.id.ivIconShuffle)
    public void onRandomClicked() {
        mJokeViewModel.clearJoke();
        getRandomJoke();
    }

    @OnClick(R.id.ivIconHome)
    public void onHomeClicked() {
        Tools.gotoMainActivity(this);
    }

    @OnClick(R.id.ivIconUpload)
    public void onUploadClicked() {
        onBackPressed();
    }

    private void initScreen() {
        Tools.setupToolbarNavigation(this, tbToolbar, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getRandomJoke() {
        mProgressDialog.show(getSupportFragmentManager(), AppConstants.TAG_PROGRESS);
        mJokeViewModel.requestRandomJoke(mSelectedCategory).observe(this, new Observer<Joke>() {
            @Override
            public void onChanged(Joke joke) {
                mSelectedJoke.setValue(joke);
                showJoke();
                mProgressDialog.dismiss();
            }
        });
    }

    private void showJoke() {
        if (mSelectedJoke.getValue() != null) {
            mBinding.setJoke(mSelectedJoke.getValue());
            Tools.setImage(mBinding.getRoot().getContext(), mBinding.ivJokeIconDetail, mSelectedJoke.getValue().getIconUrl());
        }
    }
}