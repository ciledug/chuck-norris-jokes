package id.my.farmasikita.jokesonyou.ui.joke;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.my.farmasikita.jokesonyou.R;
import id.my.farmasikita.jokesonyou.fragments.ProgressDialogFragment;
import id.my.farmasikita.jokesonyou.models.Joke;
import id.my.farmasikita.jokesonyou.utils.AppConstants;
import id.my.farmasikita.jokesonyou.utils.Tools;

public class SearchJokeActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private JokeViewModel mViewModel;
    private JokeAdapter mJokeAdapter;
    private SearchView mSearchView;
    private String mPreviousQuery = "";
    private ProgressDialogFragment mProgressDialog;
    private InputMethodManager mInputMethodManager;

    private JokeAdapter.JokeAdapterListener mListener = new JokeAdapter.JokeAdapterListener() {
        @Override
        public void onJokeClicked(Joke joke) {
            gotoJokeDetail(joke);
        }
    };

    @BindView(R.id.tbToolbar)
    Toolbar tbToolbar;

    @BindView(R.id.rvJokeListContainer)
    RecyclerView rvJokesContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke_list);
        ButterKnife.bind(this);

        mViewModel = new ViewModelProvider(this).get(JokeViewModel.class);
        mProgressDialog = ProgressDialogFragment.newInstance();

        initScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mViewModel.getJokes() != null) {
            mJokeAdapter.refreshList(mViewModel.getJokes().getValue());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem mi = menu.findItem(R.id.menu_search_container);

        setupSearchView(mi);
        return super.onCreateOptionsMenu(menu);
    }

    private void initScreen() {
        Tools.setupToolbarNavigation(this, tbToolbar, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        mJokeAdapter = new JokeAdapter(getApplicationContext(), new ArrayList<>(), mListener);
        rvJokesContainer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvJokesContainer.setAdapter(mJokeAdapter);
    }

    private void setupSearchView(MenuItem mi) {
        if (mi != null) {
            mSearchView = (SearchView) mi.getActionView();
            mSearchView.setQueryHint(getString(R.string.hint_search));
            mSearchView.setIconified(false);
            mSearchView.onActionViewExpanded();
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if ((query.length() >= 3) && !query.equalsIgnoreCase(mPreviousQuery)) {
                        mPreviousQuery = query;
                        doSearch();
                    }
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.length() > AppConstants.SEARCH_TEXT_INPUT_LENGTH) {
                        mSearchView.setQuery(newText.substring(0, AppConstants.SEARCH_TEXT_INPUT_LENGTH), false);
                    }
                    return true;
                }
            });

            if (mViewModel.getSearchQuery() != null) {
                mSearchView.setQuery(mViewModel.getSearchQuery().getValue(), false);
                mSearchView.clearFocus();
            }
        }
    }

    private void doSearch() {
        mProgressDialog.show(getSupportFragmentManager(), AppConstants.TAG_PROGRESS);
        mViewModel.clearJokeList();
        mViewModel.requestSearch(mPreviousQuery).observe(this, new Observer<List<Joke>>() {
            @Override
            public void onChanged(List<Joke> jokeList) {
                mJokeAdapter.refreshList(jokeList);
                Tools.hideKeyboard(mInputMethodManager, mSearchView);
                mProgressDialog.dismiss();
            }
        });
    }

    private void gotoJokeDetail(Joke joke) {
        Bundle b = new Bundle();
        b.putSerializable(AppConstants.EXTRA_SELECTED_JOKE, joke);

        Intent i = new Intent(this, JokeDetailActivity.class);
        i.putExtra(AppConstants.EXTRA_BUNDLE, b);
        startActivity(i);
    }
}