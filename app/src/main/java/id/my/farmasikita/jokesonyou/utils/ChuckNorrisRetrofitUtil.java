package id.my.farmasikita.jokesonyou.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import id.my.farmasikita.jokesonyou.models.CategoryList;
import id.my.farmasikita.jokesonyou.repository.deserializers.CategoryDeserializer;
import id.my.farmasikita.jokesonyou.repository.ChuckNorrisAPIService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChuckNorrisRetrofitUtil {

    private static boolean isRequestNewToken = false;
    private static OkHttpClient client;

    public static ChuckNorrisAPIService create() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(loggingInterceptor);

        client = builder
                .addInterceptor(loggingInterceptor)
                .connectTimeout(ChuckNorrisAPIService.TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(ChuckNorrisAPIService.READ_TIMEOUT, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(CategoryList.class, new CategoryDeserializer())
//                .registerTypeAdapter(Joke.class, new JokeDeserializer())
                .serializeNulls()
                .create();

        String url = ChuckNorrisAPIService.developmentURL;

        return new Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ChuckNorrisAPIService.class);
    }
}
