package id.my.farmasikita.jokesonyou.repository.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import id.my.farmasikita.jokesonyou.models.CategoryList;

public class CategoryDeserializer implements JsonDeserializer<CategoryList> {
    @Override
    public CategoryList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject categories = new JsonObject();

        if (json.isJsonArray()) {
            categories.add("categories", json);
        }

        return new Gson().fromJson(categories, CategoryList.class );
    }
}
