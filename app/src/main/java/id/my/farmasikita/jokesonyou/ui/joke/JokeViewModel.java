package id.my.farmasikita.jokesonyou.ui.joke;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import id.my.farmasikita.jokesonyou.models.Joke;
import id.my.farmasikita.jokesonyou.repository.DataRepository;

public class JokeViewModel extends ViewModel {

    private final String TAG = getClass().getSimpleName();

    private MutableLiveData<Joke> mJoke;
    private MutableLiveData<List<Joke>> mJokes;
    private DataRepository mDataRepository;
    private MutableLiveData<String> mSearchQuery;

    public JokeViewModel() {
        mDataRepository = DataRepository.getInstance();
    }

    public LiveData<Joke> requestRandomJoke(String category) {
        if (mJoke == null) {
            mJoke = mDataRepository.requestRandomJoke(category);
        }
        return mJoke;
    }

    public LiveData<List<Joke>> requestSearch(String query) {
        if (mJokes == null) {
            mJokes = mDataRepository.requestSearch(query);
            mSearchQuery = new MutableLiveData<>();
            mSearchQuery.setValue(query);
        }
        return mJokes;
    }

    public void clearJoke() {
        if (mJoke != null) {
            mJoke = null;
        }
    }

    public void clearJokeList() {
        if (mJokes != null) {
            mJokes = null;
            mSearchQuery = null;
        }
    }

    public LiveData<Joke> getJoke() {
        return mJoke;
    }

    public LiveData<List<Joke>> getJokes() { return mJokes; }

    public LiveData<String> getSearchQuery() {
        return mSearchQuery;
    }

}